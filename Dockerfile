# Usiamo una distro minimalista
FROM alpine:latest

# Installiamo python3 e pip
RUN apk add --update py3-pip

# Copiamo e installiamo le dipendenze della nosta app con pip
COPY requirements.txt /app/
RUN pip install --break-system-packages --no-cache-dir -r /app/requirements.txt

# Copiamo la nostra app
COPY src /app/

# Comunichiamo su che porta la nostra app e in ascolto
EXPOSE 5000

# Avvia l'app
CMD ["python", "/app/app.py"]
