from flask import Flask, render_template
import random

app = Flask(__name__)

# list of cat images
images = [
    "https://img.buzzfeed.com/buzzfeed-static/static/2023-11/16/10/asset/2673aedb1c24/sub-buzz-4318-1700132386-1.png",
    "https://img.buzzfeed.com/buzzfeed-static/static/2023-11/16/11/asset/d251f525e42a/sub-buzz-531-1700133822-1.png",
    "https://img.buzzfeed.com/buzzfeed-static/static/2023-11/16/11/asset/91049165722e/sub-buzz-3646-1700132466-8.jpg",
    "https://img.buzzfeed.com/buzzfeed-static/static/2023-11/16/11/asset/2673aedb1c24/sub-buzz-4391-1700134010-1.jpg",
    "https://img.buzzfeed.com/buzzfeed-static/static/2023-11/16/11/asset/d7131de1fe91/sub-buzz-2112-1700132693-1.jpg",
    "https://img.buzzfeed.com/buzzfeed-static/static/2023-11/16/11/asset/739be8a712c9/sub-buzz-2180-1700134134-1.png",
    "https://img.buzzfeed.com/buzzfeed-static/static/2023-11/16/11/asset/739be8a712c9/sub-buzz-2109-1700132758-2.jpg"
]

@app.route('/')
def index():
    url = random.choice(images)
    return render_template('index.html', url=url)

if __name__ == "__main__":
    app.run(host="0.0.0.0")
